# WhatsApp Data Analysis

Own Whatsapp data analysis using ML Algorithm, we require some libraries need to be installed.


First `Clone` this repository in your local computer.


`Install  Python` and `Anaconda` in in your local computer.

## Installation Steps are

```python
pip3 install --upgrade pip

pip3 install jupyter # jupyter is coming by default with conda.

pip install matplotlib seaborn pandas wordcloud jovian emoji
# This will install all dependent library.

```
## cd to cloned directory

```python
jovian install

conda activate whatsapp # Using this you can create your own working directory.

jupyter notebook # this command will start jupyter notebook.

```
## For Better Understanding visit
[Open Documentation](https://kibbcomindia-my.sharepoint.com/:w:/g/personal/gautam_shandilya_kibbcom_com/Ed0UIUUJepRDneDtgVMo8HcBRkPvD-vjQYQpI1LnqOAeJg?e=X0J4Cp).
